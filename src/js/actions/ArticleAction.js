var ArticleConstants 		= require('../constants/ArticleConstants'),
	ArticleDispatcher 		= require('../dispatchers/ArticleDispatcher'),
	$ 						= require('jquery');



/**
	var : ArticleAction 
	Will have functionality related to the Article , 
	As we want to load a json file , I've used Jquery.ajax here , obviously we could have used native javascript methods as well

*/	
var ArticleAction 			= {
		loadMoreArtiles: function(query){
		  	$.ajax({
		      url: ArticleConstants.JSON_SAMPLE_URL,
		      dataType: 'json',
		      cache: false,
		      success: function(data) {
		      	var newArticles = data.articles.splice(query.index,query.skip);
		      	
		      	ArticleDispatcher.handleViewAction({
					actionType:ArticleConstants.LOAD_MORE,
					data:newArticles
				});
		      },
		      error: function(xhr, status, err) {
		       console.log('error ajax : ',xhr, status, err);
		      }
		    });
	}
};

module.exports 			= ArticleAction;