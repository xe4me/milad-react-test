var Dispatcher  = require('flux').Dispatcher;
var assign = require('react/lib/Object.assign');


/**
	Dispatcher : ArticleDispatcher
	Is a dispatcher that dispatches incomming actions so any store that is listening will get the data flow
*/
var ArticleDispatcher = assign(new Dispatcher() , {

	handleViewAction : function(action){
		this.dispatch({
			source 	: 	'VIEW_ACTION',
			action 	: 	action
		});
	}
});

module.exports = ArticleDispatcher;