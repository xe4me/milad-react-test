var React = require('react');
var ArticleAction = require('../actions/ArticleAction');
var Articles = require('../components/Articles');


/**
	Articles :  App 
	Is a React Component which will be injected into the document 
*/
var App = React.createClass({
	loadedArticles : 0, // initially we want to load the first article in the list so index is 0
	skip : 3, // every click on loadMore button will load 3 new articles 
	getInitialState:function(){
		ArticleAction.loadMoreArtiles({index:this.loadedArticles,skip:this.skip});
		return null;
	},
	loadMore:function(){
		this.loadedArticles+=this.skip;
		ArticleAction.loadMoreArtiles({index:this.loadedArticles,skip:this.skip});
	},
	render: function() {
		return (
			<div>
				<h2>Articles</h2>
				<Articles/>
				<button className="btn btn-primary pull-right" onClick={this.loadMore}>Load More ...</button>`
			</div>
		);
	}
});

module.exports = App;